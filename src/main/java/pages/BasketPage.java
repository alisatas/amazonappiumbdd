package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;

public class BasketPage extends PageBase{
    public BasketPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }
    static String  getBasketPrice;

    @FindBy(id="com.amazon.mShop.android.shopping:id/cart_count")
    MobileElement setBasketTab;
    public void clickBasketTab() {
        click(setBasketTab);
    }


    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.view.View[1]/android.view.View[2]/android.view.View[2]/android.view.View[2]/android.view.View[1]")
    MobileElement productPrice;
    public String setProductPrice() {
        getBasketPrice=getAttribute(productPrice,"text");
        System.out.println("Price: " + getBasketPrice);
        return getBasketPrice;
    }
}

package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.FindBy;

public class ProductDetailPage extends PageBase{

    String price;
    public ProductDetailPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }

    @FindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[3]/android.view.View[1]/android.view.View[2]/android.view.View/android.view.View[2]")
    MobileElement productPrice;

    @FindBy(xpath="//*[@resource-id='add-to-cart-button']")
    MobileElement addToCartButton;

    @FindBy(xpath="//android.view.View[@content-desc=\"Heart to save an item to your default list\"]")
    MobileElement favoriteButton;

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]")
    MobileElement container;

    @FindBy(xpath="//*[@resource-id='exports_mobileapp_qualifiedBuybox_availabilityInsideBuyBox_feature_div']")
    MobileElement InStockArea;



    public String setProductPrices() {
        price=getAttribute(productPrice,"text");
        System.out.println("Price: " + price);
        return price;
    }

    public void clickAddCartButton() {
        click(addToCartButton);

    }

    public void swipeElementsToPriceArea() throws InterruptedException {
        swipeElements(favoriteButton,container);
    }

    public void swipesToAddToCartButton() throws InterruptedException {
        swipeElements(productPrice,InStockArea);
    }
}

package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;

public class SplashPage extends PageBase{
    public SplashPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }


    @FindBy(id="com.amazon.mShop.android.shopping:id/btn_cancel")
    MobileElement cancelButton;

    @FindBy(id="com.amazon.mShop.android.shopping:id/sign_in_button")
    MobileElement signInButton;

    @FindBy(id="com.amazon.mShop.android.shopping:id/new_user")
    MobileElement createAccountButton;

    @FindBy(id="com.amazon.mShop.android.shopping:id/skip_sign_in_button")
    MobileElement skipSignInButton;

    @FindBy(id="com.amazon.mShop.android.shopping:id/sso_splash_logo")
    MobileElement amazonLogo;

    @FindBy(id="com.amazon.mShop.android.shopping:id/signin_to_yourAccount")
    MobileElement mainTitle;

    @FindBy(id="com.amazon.mShop.android.shopping:id/view_your_wish_list")
    MobileElement firstSubTitle;

    @FindBy(id="com.amazon.mShop.android.shopping:id/Find_purchase")
    MobileElement secondSubTitle2;

    @FindBy(id="com.amazon.mShop.android.shopping:id/track_your_packages")
    MobileElement thirdSubTitle3;


    public void setCancelButton()  {
        click(cancelButton);
    }

    public void elementIsDisplayedOnSplashPage(){
        isElementDisplayed(signInButton);
        isElementDisplayed(createAccountButton);
        isElementDisplayed(skipSignInButton);
        isElementDisplayed(amazonLogo);
    }

    public void getTextMainTitle(String expectedText){
        String getMainTitle = getAttribute(mainTitle, "text");
        controlAssertionEqual(getMainTitle, expectedText);
    }

    public void getTextFirstSubTitle(String expectedFirstSubTitle1) {
        String getTextFirstSubTitle = getAttribute(firstSubTitle, "text");
        controlAssertionEqual(getTextFirstSubTitle, expectedFirstSubTitle1);
    }

    public void getTextSecondSubTitle(String secondSubTitle) {
        String getTextSecondSubTitle = getAttribute(secondSubTitle2, "text");
        controlAssertionEqual(getTextSecondSubTitle,secondSubTitle );
    }

    public void getThirdSubTitle(String thirdSubTitle) {
        String getTextThirdSubTitle = getAttribute(thirdSubTitle3, "text");
        controlAssertionEqual(getTextThirdSubTitle,thirdSubTitle );
    }

    public void setSkipSignInButton() {
        click(skipSignInButton);
    }
}

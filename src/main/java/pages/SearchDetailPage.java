package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;

public class SearchDetailPage extends PageBase {

    public SearchDetailPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }

    @FindBy(id="com.amazon.mShop.android.shopping:id/rs_search_src_text")
    MobileElement searchAreaForProducts;

    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView")
    MobileElement firstResultOfProducts;

    @FindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View[3]")
    MobileElement firstOptionResultList;


    public void writeProductNameTpSearchArea(String productName) {sendText(searchAreaForProducts,productName);}

    public void clickFirstResult() {click(firstResultOfProducts);}

    public void chooseFirstOptionOnList() {
        click(firstOptionResultList);
    }
}

package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase{
    public HomePage(AppiumDriver appiumDriver) {super(appiumDriver);}

    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[1]")
    MobileElement homeButton;

    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[2]")
    MobileElement accountButton;

    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[3]")
    MobileElement basketButton;

    @FindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[4]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/androidx.appcompat.app.ActionBar.Tab[4]")
    MobileElement menuButton;

    @FindBy(id="com.amazon.mShop.android.shopping:id/chrome_action_bar_search_disabled")
    MobileElement searchIconButton;

    @FindBy(xpath="(//android.widget.LinearLayout[@content-desc='Search'])[1]/android.widget.LinearLayout/android.widget.TextView")
    MobileElement searchAreaHomePage;

    @FindBy(id="com.amazon.mShop.android.shopping:id/chrome_action_bar_search_icon")
    MobileElement searchIconButtonForSearch;

    public void controlHomeButton() {isElementDisplayed(homeButton);}

    public void controlAccountButton() {isElementDisplayed(accountButton);}

    public void controlBasketButton() {isElementDisplayed(basketButton);}

    public void controlMenuButton() {isElementDisplayed(menuButton);}

    public void controlSearchIconInSearchField() {isElementDisplayed(searchIconButton);}

    public void controlSearchArea() {isElementDisplayed(searchAreaHomePage);}

    public void clickSearchArea() {click(searchAreaHomePage);}

    public void clickSearchIconButton() {click(searchIconButtonForSearch);}

}

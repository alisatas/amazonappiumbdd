package AmazonFiles.steps;

import AmazonFiles.base.TestBase;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.HomePage;

public class HomePageStepDefinition extends TestBase {
    HomePage homePage;
    @Then("user controls home button")
    public void userControlsHomeButton() {
        homePage=new HomePage(driver);
        homePage.controlHomeButton();

    }

    @Then("user controls account button")
    public void userControlsAccountButton() {
        homePage=new HomePage(driver);
        homePage.controlAccountButton();
    }

    @Then("user controls basket button")
    public void userControlsBasketButton() {
        homePage=new HomePage(driver);
        homePage.controlBasketButton();
    }

    @Then("user controls menu button")
    public void userControlsMenuButton() {
        homePage=new HomePage(driver);
        homePage.controlMenuButton();
    }

    @Then("user control search icon in search field")
    public void userControlSearchIconInSearchField() {
        homePage=new HomePage(driver);
        homePage.controlSearchIconInSearchField();
    }

    @Then("user controls search area")
    public void userControlsSearchArea() {
        homePage=new HomePage(driver);
        homePage.controlSearchArea();
    }

    @Given("User clicks search area")
    public void userClicksSearchArea() {
        homePage=new HomePage(driver);
        homePage.clickSearchArea();

    }
    @Given("User clicks searchIcon button")
    public void userClicksSearchIconButton() {
        homePage=new HomePage(driver);
        homePage.clickSearchIconButton();
    }

}

package AmazonFiles.steps;

import AmazonFiles.base.TestBase;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import pages.SearchDetailPage;

public class SearchDetailStepDefinition extends TestBase {
    SearchDetailPage searchDetailPage;
    @When("user writes {string} for select product")
    public void userWritesForSelectProduct(String productName) {
        searchDetailPage=new SearchDetailPage(driver);
        searchDetailPage.writeProductNameTpSearchArea(productName);
    }
    @And("User clicks first result on searched object")
    public void userClicksFirstResult() {
        searchDetailPage=new SearchDetailPage(driver);
        searchDetailPage.clickFirstResult();
    }

    @And("user selects first option of the results")
    public void userSelectsFirstOptionOfTheResults() {
        searchDetailPage=new SearchDetailPage(driver);
        searchDetailPage.chooseFirstOptionOnList();
    }
}

package AmazonFiles.steps;

import AmazonFiles.base.TestBase;
import io.cucumber.java.en.And;
import pages.HomePage;
import pages.ProductDetailPage;

public class ProductDetailStepDefinition extends TestBase {

    ProductDetailPage productDetailPage;

    @And("user control price of the product")
    public void userControlPriceOfTheProduct() {
        productDetailPage=new ProductDetailPage(driver);
        productDetailPage.setProductPrices();

    }

    @And("user clicks to add cart button")
    public void userClicksToAddBasketButton() {
        productDetailPage=new ProductDetailPage(driver);
        productDetailPage.clickAddCartButton();
    }


    @And("users swipes to price area")
    public void usersSwipesToElement() throws InterruptedException {
        productDetailPage=new ProductDetailPage(driver);
        productDetailPage.swipeElementsToPriceArea();
    }

    @And("user control price for product")
    public void userControlPriceForProduct() {
        productDetailPage.setProductPrices();
    }

    @And("user clicks add basket button")
    public void userClicksAddBasketButton() {
        productDetailPage.clickAddCartButton();
    }

    @And("user swipes to add to card area")
    public void userSwipesToAddToCardArea() throws InterruptedException {
        productDetailPage.swipesToAddToCartButton();
    }
}

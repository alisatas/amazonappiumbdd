package AmazonFiles.steps;

import AmazonFiles.base.TestBase;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pages.BasketPage;
import pages.ProductDetailPage;

public class BasketStepDefinition  extends TestBase {
    BasketPage basketPage;
    @And("user goes to basket tab")
    public void userGoesToAddBasketTab() {
        basketPage.clickBasketTab();
    }
}

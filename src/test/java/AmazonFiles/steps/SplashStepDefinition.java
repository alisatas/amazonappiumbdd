package AmazonFiles.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import AmazonFiles.base.TestBase;
import pages.SplashPage;

public class SplashStepDefinition extends TestBase {
    SplashPage splashPage;


    @When("user clicks cancel button")
    public void user_clicks_cancel_button() {
        splashPage= new SplashPage(driver);
        splashPage.setCancelButton();
    }

    @Then("user control buttons on splash page")
    public void userControlSignInButton() throws InterruptedException {
        splashPage= new SplashPage(driver);
        Thread.sleep(2000);
        splashPage.elementIsDisplayedOnSplashPage();
    }

    @Then("User control main title {string} on splash page")
    public void userControlMainTitleOnSplashPage(String title) {
        splashPage= new SplashPage(driver);
        splashPage.getTextMainTitle(title);
    }


    @Then("User control first subtitle {string} on splash page")
    public void userControlFirstSubtitleOnSplashPage(String firstSubTitle) {
        splashPage= new SplashPage(driver);
        splashPage.getTextFirstSubTitle(firstSubTitle);
    }

    @Then("User control second subtitle {string} on splash page")
    public void userControlSecondSubtitleOnSplashPage(String secondSubTitle) {
        splashPage= new SplashPage(driver);
        splashPage.getTextSecondSubTitle(secondSubTitle);
    }

    @Then("User control third subtitle {string} on splash page")
    public void userControlThirdSubtitleOnSplashPage(String thirdSubTitle) {
        splashPage=new SplashPage(driver);
        splashPage.getThirdSubTitle(thirdSubTitle);
    }

    @Given("user clicks skipSignIn button")
    public void userClicksSkipSignInButton() throws InterruptedException {
        splashPage=new SplashPage(driver);
        Thread.sleep(2000);
        splashPage.setSkipSignInButton();
    }
}
Feature: Android Login

  Background:Before Home Page
    When user clicks cancel button
    Given user clicks skipSignIn button

  @Android @ControlHomePageItems
  Scenario: Controlling home page buttons
    Then user controls home button
    Then user controls account button
    Then user controls basket button
    Then user controls menu button
    Then user control search icon in search field
    Then user controls search area

  @Android @SearchForProduct
  Scenario: User searches for a product
    Given User clicks searchIcon button
    When user writes "iphone 13 pro max phone" for select product
    And User clicks first result on searched object
    And user selects first option of the results
    And users swipes to price area
    And user control price for product
    And user swipes to add to card area
    And user clicks add basket button














